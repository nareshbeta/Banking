<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
  <head>  
    <title>Admin Home</title>  
    <style>
      .username.ng-valid {
          background-color: lightgreen;
      }
      .username.ng-dirty.ng-invalid-required {
          background-color: red;
      }
      .username.ng-dirty.ng-invalid-minlength {
          background-color: yellow;
      }

      .email.ng-valid {
          background-color: lightgreen;
      }
      .email.ng-dirty.ng-invalid-required {
          background-color: red;
      }
      .email.ng-dirty.ng-invalid-email {
          background-color: yellow;
      }

    </style>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
     <link rel="stylesheet" href="../css/app.css">
  </head>
  <body ng-app="myApp">
      <div class="generic-container" ng-controller="UserController as ctrl">
          <div class="panel panel-default">
              <div class="panel-heading">
              <span class="lead">Admin Registration Form </span>
              <!-- <a onclick="return map();" style="text-decoration: none;">Map</a> -->
              
              <a href="${pageContext.request.contextPath}/logout" style="text-decoration: none;">Sign Out</a>
              
              </div>
              <div class="formcontainer">
                  <form ng-submit="ctrl.submit()" name="myForm" class="form-horizontal">
                      <input type="hidden" ng-model="ctrl.user.id" />
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="uname">Name</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.user.name" id="name" class="username form-control input-sm" placeholder="Enter name" required ng-minlength="3"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.name.$error.required">This is a required field</span>
                                      <span ng-show="myForm.name.$error.minlength">Minimum length required is 3</span>
                                      <span ng-show="myForm.name.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="idNumber">Id number</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.user.idNumber" id="idNumber" class="username form-control input-sm" placeholder="Enter Id number" required ng-minlength="3"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.idNumber.$error.required">This is a required field</span>
                                      <span ng-show="myForm.idNumber.$error.minlength">Minimum length required is 3</span>
                                      <span ng-show="myForm.idNumber.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="address">Address</label>
                              <div class="col-md-7">
                                  <input type="text" ng-model="ctrl.user.address" id="address" class="username form-control input-sm" placeholder="Enter Address" required ng-minlength="3"/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.address.$error.required">This is a required field</span>
                                      <span ng-show="myForm.address.$error.minlength">Minimum length required is 3</span>
                                      <span ng-show="myForm.address.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>

                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="salary">Account Number</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.user.accNo" id="accNo" class="form-control input-sm" placeholder="Enter your Account Number" ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" required/>
                                  <div class="has-error" ng-show="myForm.$dirty">
                                      <span ng-show="myForm.accNo.$error.required">This is a required field</span>
                                      <span ng-show="myForm.accNo.$invalid">This field is invalid </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                      
                      <div class="row">
                          <div class="form-group col-md-12">
                              <label class="col-md-2 control-lable" for="salary">Balance</label>
                              <div class="col-md-7">
                                  <input type="number" ng-model="ctrl.user.balance" id="balance" class="form-control input-sm" placeholder="Enter Balance"/>
                              </div>
                          </div>
                      </div>

                      <div class="row">
                          <div class="form-actions floatRight">
                              <input type="submit"  value="{{!ctrl.user.id ? 'Add' : 'Update'}}" class="btn btn-primary btn-sm" ng-disabled="myForm.$invalid">
                              <button type="button" ng-click="ctrl.reset()" class="btn btn-warning btn-sm" ng-disabled="myForm.$pristine">Reset Form</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">List of Users </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>Name</th>
                              <th>Id Number</th>
                              <th>Address</th>
                              <th>Account Number</th>
                              <th>Balance</th>
                              <th width="100">
                          </tr>
                      </thead>
                      <tbody>
                          <tr ng-repeat="u in ctrl.users">
                              <td><span ng-bind="u.name"></span></td>
                              <td><span ng-bind="u.idNumber"></span></td>
                              <td><span ng-bind="u.address"></span></td>
                              <td><span ng-bind="u.accNo"></span></td>
                              <td><span ng-bind="u.balance"></span></td>
                              <td>
                              <button type="button" ng-click="ctrl.edit(u.id)" class="btn btn-success custom-width">Edit</button>
                              <!-- <button type="button" ng-click="ctrl.remove(u.id)" class="btn btn-danger custom-width">Remove</button> -->
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>  
      <script src="../js/app.js"></script>
      <script src="../js/authInterceptor.js"></script>
      <script src="../js/user_service.js"></script>
      <script src="../js/user_controller.js"></script>
  </body>
</html>
var cities = [{
  city: ' 47100 Puchong',
  desc: '2, Jalan Putra Perdana 3/23, Taman Putra Perdana, 47100 Puchong, Selangor',
  lat: 3.02558454,
  long: 102.6138363
},
{
  city: ' Puchong',
  desc: '1, Jalan Putra Perdana 8/2, Taman Putra Perdana, 47100 Puchong, Selangor, Selangor',
  lat: 3.0256988,
  long: 101.6138363
},
{
  city: '101.304146',
  desc: 'Ceria Residence, Jalan Ceria 1C, Cyberjaya, 63000 Cyberjaya, Selangor',
  lat: 3.2083304,
  long: 101.304146
},
{
  city: ' Puchong',
  desc: '1, Jalan Putra Perdana 8/2, Taman Putra Perdana, 47100 Puchong, Selangor, Selangor',
  lat: 3.0256988,
  long: 101.6138363
},
{
  city: '101.304146',
  desc: 'Ceria Residence, Jalan Ceria 1C, Cyberjaya, 63000 Cyberjaya, Selangor',
  lat: 3.2083304,
  long: 101.304146
}
];

//Create angular controller.
var app = angular.module('googleAapApp', []);
app.controller('googleAapCtrl', function($scope) {
  $scope.highlighters = [];
  $scope.gMap = null;
 
  var winInfo = new google.maps.InfoWindow();
 
  var googleMapOption = {
    zoom: 4,
    center: new google.maps.LatLng(25, 80),
    mapTypeId: google.maps.MapTypeId.TERRAIN
  };

  $scope.gMap = new google.maps.Map(document.getElementById('googleMap'), googleMapOption);

 

  var createHighlighter = function(citi) {

    var citiesInfo = new google.maps.Marker({
      map: $scope.gMap,
      position: new google.maps.LatLng(citi.lat, citi.long),
      title: citi.city
    });

    citiesInfo.content = '<div>' + citi.desc + '</div>';

    google.maps.event.addListener(citiesInfo, 'click', function() {
      winInfo.setContent('<h1>' + citiesInfo.title + '</h1>' + citiesInfo.content);
      winInfo.open($scope.gMap, citiesInfo);
    });
    $scope.highlighters.push(citiesInfo);
  };

  for (i = 0; i < cities.length; i++) {
    createHighlighter(cities[i]);
  }
});


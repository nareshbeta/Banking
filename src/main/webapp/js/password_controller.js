'use strict';

angular.module('myApp').controller('PasswordController', ['$scope', 'PasswordService', function($scope, PasswordService) {
    var self = this;
    self.user={id:null,username:'',address:'',email:''};
    self.users=[];
    self.accDetails={};
    self.transactions = [];

    self.submit = submit;
    self.edit = edit;
    self.remove = remove;
    self.reset = reset;
    self.updateProfile = updateProfile;
    self.transfer = transfer;
    self.resetTransfer = resetTransfer;
    self.fetchUserProfile = fetchUserProfile;
    self.fetchAllTransactions = fetchAllTransactions;
    
    self.passwordForm = {};
    
    $scope.tab = 1;

    $scope.setTab = function(newTab){
    	$scope.transferSuccessMessageShow=false;
        $scope.transferErrorAccountNotExistsShow=false;
        $scope.transferErrorInsufficientFundsShow=false;
      $scope.tab = newTab;
    };
    
    $scope.checkFormFirstPage=true;
    
    $scope.transferSuccessMessageShow=false;
    $scope.transferErrorAccountNotExistsShow=false;
    $scope.transferErrorInsufficientFundsShow=false;

    $scope.isSet = function(tabNum){
      return $scope.tab === tabNum;
    };
    
    $scope.readOnly=true;
    
    $scope.setProfileToggling = function(){
    	$scope.readOnly=false;
    };
    

    
    
    function transfer() {
    	transferMoney(self.accDetails);
    }
    
    function transferMoney(accDetails){
    	PasswordService.transfer(accDetails).then(function(response){
    		$scope.transferSuccessMessageShow=response.success;
    		$scope.transferErrorAccountNotExistsShow=response.accountNotExists;
    	    $scope.transferErrorInsufficientFundsShow=response.insufficientFunds;
    		if(response.success){
    			fetchUserProfile();
    			fetchAllTransactions();
    			resetTransfer();
    		}
    	});
    }
    
    $scope.passwordSuccessMessage = false;
    
    function updateProfile(user) {
    	PasswordService.updateUser(user);
    	$scope.passwordSuccessMessage = true;
    }
    
    function createUser(user){
    	PasswordService.createUser(user)
            .then(
            function(response){
            	console.log(response);
            	if(response){
            		$scope.checkFormFirstPage=false;
            	}
            	else{
            		//error message
            		alert("Invalid details");
            	}
            	
            },
            function(errResponse){
                console.error('Error while creating User');
            }
        );
    }
    

    //fetchAllTransactions();
    //fetchUserProfile();
    
    function fetchUserProfile(){
    	PasswordService.profile()
            .then(
            function(d) {
                self.user = d;
            },
            function(errResponse){
                console.error('Error while fetching Users');
            }
        );
    }
    

    function fetchAllTransactions(){
    	PasswordService.fetchAllTxns()
            .then(
            function(d) {
                self.transactions = d;
            },
            function(errResponse){
                console.error('Error while fetching Users');
            }
        );
    }

    /*function createUser(user){
    	PasswordService.createUser(user)
            .then(
            function(response)
            {
            	$scopr.checkFormFirstPage=false;
            	console.log(response);
            },
            function(errResponse){
                console.error('Error while creating User');
            }
        );
    }*/

    function updateUser(user, id){
    	PasswordService.updateUser(user, id)
            .then(
            function(response){
            	
            },
            function(errResponse){
                console.error('Error while updating User');
            }
        );
    }

    function deleteUser(id){
    	PasswordService.deleteUser(id)
            .then(
            fetchAllUsers,
            function(errResponse){
                console.error('Error while deleting User');
            }
        );
    }

    function submit() {
        if($scope.checkFormFirstPage){
            console.log('Saving New User', self.passwordForm);
            createUser(self.passwordForm);
        }
        else{
        	//update password
        	updateProfile(self.passwordForm);
        }
    }

    function edit(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.users.length; i++){
            if(self.users[i].id === id) {
                self.user = angular.copy(self.users[i]);
                break;
            }
        }
    }

    function remove(id){
        console.log('id to be deleted', id);
        if(self.user.id === id) {//clean form if the user to be deleted is shown there.
            reset();
        }
        deleteUser(id);
    }


    function reset(){
        self.user={id:null,username:'',address:'',email:''};
        $scope.myForm.$setPristine(); //reset Form
    }
    
    function resetTransfer(){
    	self.accDetails={};
    }

}]);

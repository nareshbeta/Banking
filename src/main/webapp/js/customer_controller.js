'use strict';

angular.module('myApp').controller('CustomerController', ['$scope', 'CustomerService', function($scope, CustomerService) {
    var self = this;
    self.user={id:null,username:'',address:'',email:''};
    self.users=[];
    self.accDetails={};
    self.transactions = [];

    self.submit = submit;
    self.edit = edit;
    self.remove = remove;
    self.reset = reset;
    self.updateProfile = updateProfile;
    self.transfer = transfer;
    self.resetTransfer = resetTransfer;
    self.fetchUserProfile = fetchUserProfile;
    self.fetchAllTransactions = fetchAllTransactions;
    
    $scope.tab = 1;

    $scope.setTab = function(newTab){
    	$scope.transferSuccessMessageShow=false;
        $scope.transferErrorAccountNotExistsShow=false;
        $scope.transferErrorInsufficientFundsShow=false;
      $scope.tab = newTab;
    };
    
    $scope.transferSuccessMessageShow=false;
    $scope.transferErrorAccountNotExistsShow=false;
    $scope.transferErrorInsufficientFundsShow=false;

    $scope.isSet = function(tabNum){
      return $scope.tab === tabNum;
    };
    
    $scope.readOnly=true;
    
    $scope.setProfileToggling = function(){
    	$scope.readOnly=false;
    };
    

    
    
    function transfer() {
    	transferMoney(self.accDetails);
    }
    
    function transferMoney(accDetails){
    	CustomerService.transfer(accDetails).then(function(response){
    		$scope.transferSuccessMessageShow=response.success;
    		$scope.transferErrorAccountNotExistsShow=response.accountNotExists;
    	    $scope.transferErrorInsufficientFundsShow=response.insufficientFunds;
    		if(response.success){
    			fetchUserProfile();
    			fetchAllTransactions();
    			resetTransfer();
    		}
    	});
    }
    
    function updateProfile(user) {
    	CustomerService.updateUser();
    	createUser(self.user, self.user.id);
       $scope.readOnly=true;
    }
    
    function createUser(user){
    	CustomerService.createUser(user)
            .then(
            fetchUserProfile,
            function(errResponse){
                console.error('Error while creating User');
            }
        );
    }
    

    fetchAllTransactions();
    fetchUserProfile();
    
    function fetchUserProfile(){
    	CustomerService.profile()
            .then(
            function(d) {
                self.user = d;
            },
            function(errResponse){
                console.error('Error while fetching Users');
            }
        );
    }
    

    function fetchAllTransactions(){
    	CustomerService.fetchAllTxns()
            .then(
            function(d) {
                self.transactions = d;
            },
            function(errResponse){
                console.error('Error while fetching Users');
            }
        );
    }

    function createUser(user){
    	CustomerService.createUser(user)
            .then(
            fetchAllUsers,
            function(errResponse){
                console.error('Error while creating User');
            }
        );
    }

    function updateUser(user, id){
    	CustomerService.updateUser(user, id)
            .then(
            fetchAllUsers,
            function(errResponse){
                console.error('Error while updating User');
            }
        );
    }

    function deleteUser(id){
    	CustomerService.deleteUser(id)
            .then(
            fetchAllUsers,
            function(errResponse){
                console.error('Error while deleting User');
            }
        );
    }

    function submit() {
        if(self.user.id===null){
            console.log('Saving New User', self.user);
            createUser(self.user);
        }else{
            updateUser(self.user, self.user.id);
            console.log('User updated with id ', self.user.id);
        }
        reset();
    }

    function edit(id){
        console.log('id to be edited', id);
        for(var i = 0; i < self.users.length; i++){
            if(self.users[i].id === id) {
                self.user = angular.copy(self.users[i]);
                break;
            }
        }
    }

    function remove(id){
        console.log('id to be deleted', id);
        if(self.user.id === id) {//clean form if the user to be deleted is shown there.
            reset();
        }
        deleteUser(id);
    }


    function reset(){
        self.user={id:null,username:'',address:'',email:''};
        $scope.myForm.$setPristine(); //reset Form
    }
    
    function resetTransfer(){
    	self.accDetails={};
    }

}]);

'use strict';

angular.module('myApp').factory('PasswordService', ['$http', '$q', function($http, $q){

    var REST_SERVICE_URI = 'http://localhost:8080/banking';

    var factory = {
        fetchAllTxns: fetchAllTxns,
        createUser: createUser,
        updateUser:updateUser,
        deleteUser:deleteUser,
        profile:profile,
        transfer:transfer
    };

    return factory;

    function fetchAllTxns() {
        var deferred = $q.defer();
        $http.get(REST_SERVICE_URI+"/allTxns")
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while fetching Users');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    function createUser(user) {
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI+"/checkAccnoNMyKad", user)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while creating User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function transfer(accountDetails) {
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI+"/transfer", accountDetails)
            .then(
            function (response) {
            	console.log(response);
            	deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while creating User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }
    
    function profile(){
    	var deferred = $q.defer();
    	$http.get(REST_SERVICE_URI+"/profile")
    	.then(
    			function (response) {
                    deferred.resolve(response.data);
                },
                function(errResponse){
                    console.error('Error while fetching Users');
                    deferred.reject(errResponse);
                }	
    		);
    	return deferred.promise;
    }


    function updateUser(user) {
        var deferred = $q.defer();
        $http.post(REST_SERVICE_URI+"/updatePassword", user)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while updating User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

    function deleteUser(id) {
        var deferred = $q.defer();
        $http.delete(REST_SERVICE_URI+id)
            .then(
            function (response) {
                deferred.resolve(response.data);
            },
            function(errResponse){
                console.error('Error while deleting User');
                deferred.reject(errResponse);
            }
        );
        return deferred.promise;
    }

}]);

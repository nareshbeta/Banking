<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<title>Customer Home</title>
<style>
.username.ng-valid {
	background-color: lightgreen;
}

.username.ng-dirty.ng-invalid-required {
	background-color: red;
}

.username.ng-dirty.ng-invalid-minlength {
	background-color: yellow;
}

.email.ng-valid {
	background-color: lightgreen;
}

.email.ng-dirty.ng-invalid-required {
	background-color: red;
}

.email.ng-dirty.ng-invalid-email {
	background-color: yellow;
}
</style>

<style>
@import "compass/css3";

body {
	margin: 15px;
}

/* text recolor */
h1, p, a {
	color: #4DC9C9 !important;
}

/* button recolor */
.nav-pills>li.active>a, .btn-primary {
	background-color: #6C6C6C !important; // feeling like it's a rounded
	corners kind of day border-color : #6C6C6C !important;
	border-radius: 25px;
}
</style>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="../css/app.css">

<script
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.4/angular.js"></script>
<script src="../js/app.js"></script>
<script src="../js/authInterceptor.js"></script>
<script src="../js/customer_service.js"></script>
<script src="../js/customer_controller.js"></script>

</head>

<script>
 
 </script>


<body>
	<div class="container" ng-app="myApp">
		<div class="row" ng-controller="CustomerController as ctrl">
			<div class="col-md-2">
				<ul class="nav nav-pills nav-stacked">
					<li ng-class="{ active: isSet(1) }"><a href
						ng-click="setTab(1)">Profile</a></li>
					<li ng-class="{ active: isSet(2) }"><a href
						ng-click="setTab(2)">Transfer Money</a></li>
					<li ng-class="{ active: isSet(3) }"><a href
						ng-click="setTab(3)">Transaction History</a></li>
					<li ng-class="{ active: isSet(4) }"><a
						href="${pageContext.request.contextPath}/logout">Logout</a></li>
				</ul>
			</div>
			<div class="col-md-8">
				<div class="jumbotron">

					<div ng-show="isSet(1)">
						<div class="generic-container">
							<div class="panel panel-default">
								<div class="panel-heading">
									<span class="lead">Profile</span> <a class="btn btn-sm"
										ng-show="readOnly" ng-click="setProfileToggling()">Edit</a>
								</div>
								<div class="formcontainer">
									<form ng-submit="ctrl.updateProfile()" name="myForm"
										class="form-horizontal">
										<input type="hidden" ng-model="ctrl.user.id" />

										<div class="row">
											<div class="form-group col-md-12">
												<label class="col-md-2 control-lable" for="uname">Name</label>
												<div class="col-md-7">
													<input type="text" ng-readonly="readOnly"
														ng-model="ctrl.user.name" id="name"
														class="username form-control input-sm"
														placeholder="Enter name" required ng-minlength="3" />
													<div class="has-error" ng-show="myForm.$dirty">
														<span ng-show="myForm.name.$error.required">This is
															a required field</span> <span
															ng-show="myForm.name.$error.minlength">Minimum
															length required is 3</span> <span ng-show="myForm.name.$invalid">This
															field is invalid </span>
													</div>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="form-group col-md-12">
												<label class="col-md-2 control-lable" for="idNumber">Passport
													no/MyKad</label>
												<div class="col-md-7">
													<input type="text" ng-readonly="readOnly"
														ng-model="ctrl.user.idNumber" id="idNumber"
														class="username form-control input-sm"
														placeholder="Enter your Id number" required
														ng-minlength="3" />
													<div class="has-error" ng-show="myForm.$dirty">
														<span ng-show="myForm.idNumber.$error.required">This
															is a required field</span> <span
															ng-show="myForm.idNumber.$error.minlength">Minimum
															length required is 3</span> <span
															ng-show="myForm.idNumber.$invalid">This field is
															invalid </span>
													</div>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="form-group col-md-12">
												<label class="col-md-2 control-lable" for="address">Address</label>
												<div class="col-md-7">
													<input type="text" ng-readonly="readOnly"
														ng-model="ctrl.user.address" id="address"
														class="username form-control input-sm"
														placeholder="Enter your Address" required ng-minlength="3" />
													<div class="has-error" ng-show="myForm.$dirty">
														<span ng-show="myForm.address.$error.required">This
															is a required field</span> <span
															ng-show="myForm.address.$error.minlength">Minimum
															length required is 3</span> <span
															ng-show="myForm.address.$invalid">This field is
															invalid </span>
													</div>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="form-group col-md-12">
												<label class="col-md-2 control-lable" for="salary">Account
													Number</label>
												<div class="col-md-7">
													<input type="number" ng-readonly=true
														ng-model="ctrl.user.accNo" id="accNo"
														class="form-control input-sm"
														placeholder="Enter your Account Number"
														ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01"
														required />
													<div class="has-error" ng-show="myForm.$dirty">
														<span ng-show="myForm.accNo.$error.required">This
															is a required field</span> <span ng-show="myForm.accNo.$invalid">This
															field is invalid </span>
													</div>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="form-group col-md-12">
												<label class="col-md-2 control-lable" for="salary">Balance</label>
												<div class="col-md-7">
													<input type="number" ng-readonly=true
														ng-model="ctrl.user.balance" id="balance"
														class="form-control input-sm"
														placeholder="Enter your Balance"
														ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01"
														required />
													<div class="has-error" ng-show="myForm.$dirty">
														<span ng-show="myForm.balance.$error.required">This
															is a required field</span> <span
															ng-show="myForm.balance.$invalid">This field is
															invalid </span>
													</div>
												</div>
											</div>
										</div>

										<div class="row">
											<div class="form-actions floatRight">
												<input type="submit" ng-show="!readOnly"
													value="{{!ctrl.user.id ? 'Add' : 'Update'}}"
													class="btn btn-primary btn-sm">
											</div>
										</div>

									</form>
								</div>
							</div>
						</div>
					</div>





					<div ng-show="isSet(2)">
						<div class="generic-container">
						
							<div class="panel panel-default">
								<span class="lead" ng-show="transferSuccessMessageShow">Amount Transferred Successfully</span>
								<span class="lead" ng-show="transferErrorAccountNotExistsShow">Account Doesn't exist</span>
								<span class="lead" ng-show="transferErrorInsufficientFundsShow">Insufficient Funds</span>
							</div>
						
							<div class="panel panel-default">
								<div class="panel-heading">
									<span class="lead">Transfer money</span>
								</div>
							</div>

							<div class="formcontainer">
								<form ng-submit="ctrl.transfer()" name="myForm"
									class="form-horizontal">

									<div class="row">
										<div class="form-group col-md-12">
											<label class="col-md-2 control-lable" for="salary">Account
												Number</label>
											<div class="col-md-7">
												<input type="number" ng-model="ctrl.accDetails.accNo" id="accNo"
													class="form-control input-sm"
													placeholder="Enter Account Number"
													ng-pattern="/^[0-9]+(\.[0-9]{1,2})?$/" step="0.01" required />
												<div class="has-error" ng-show="myForm.$dirty">
													<span ng-show="myForm.accNo.$error.required">This is
														a required field</span> <span ng-show="myForm.accNo.$invalid">This
														field is invalid </span>
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="form-group col-md-12">
											<label class="col-md-2 control-lable" for="salary">Balance</label>
											<div class="col-md-7">
												<input type="number" ng-model="ctrl.accDetails.amount"
													id="amount" class="form-control input-sm"
													placeholder="Enter Balance" required/>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="form-actions floatRight">
											<input type="submit"
												value="Transfer"
												class="btn btn-primary btn-sm">
										</div>
									</div>


								</form>
							</div>
							s
						</div>
					</div>



					<div ng-show="isSet(3)">
						<div class="generic-container">
          <div class="panel panel-default">
                <!-- Default panel contents -->
              <div class="panel-heading"><span class="lead">List of Transactions </span></div>
              <div class="tablecontainer">
                  <table class="table table-hover">
                      <thead>
                          <tr>
                              <th>id</th>
                              <th>accNo</th>
                              <th>timeStamp</th>
                              <th>amount</th>
                              
                          </tr>
                      </thead>
                      <tbody>
                          <tr ng-repeat="u in ctrl.transactions">
                              <td><span ng-bind="u.id"></span></td>
                              <td><span ng-bind="u.accNo"></span></td>
                              <td><span ng-bind="u.timeStamp"></span></td>
                              <td><span ng-bind="u.amount"></span></td>
                          </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
					</div>
				</div>
			</div>
		</div>
</body>
</html>
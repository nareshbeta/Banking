package com.banking.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TransactionDetailsModel {
	boolean success;
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public boolean isAccountNotExists() {
		return accountNotExists;
	}
	public void setAccountNotExists(boolean accountNotExists) {
		this.accountNotExists = accountNotExists;
	}
	public boolean isInsufficientFunds() {
		return insufficientFunds;
	}
	public void setInsufficientFunds(boolean insufficientFunds) {
		this.insufficientFunds = insufficientFunds;
	}
	boolean accountNotExists;
	boolean insufficientFunds;
}

package com.banking.model;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Mail {
	String to;
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getFtlFileName() {
		return ftlFileName;
	}
	public void setFtlFileName(String ftlFileName) {
		this.ftlFileName = ftlFileName;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public Map<String, String> getModel() {
		return model;
	}
	public void setModel(Map<String, String> model) {
		this.model = model;
	}
	public Map<String, String> getInlinesMap() {
		return inlinesMap;
	}
	public void setInlinesMap(Map<String, String> inlinesMap) {
		this.inlinesMap = inlinesMap;
	}
	public Map<String, String> getAttachmentsMap() {
		return attachmentsMap;
	}
	public void setAttachmentsMap(Map<String, String> attachmentsMap) {
		this.attachmentsMap = attachmentsMap;
	}
	String ftlFileName;
	String subject;
	Map<String, String> model;//place holders
	Map<String,String> inlinesMap; //adding image src
	Map<String,String> attachmentsMap; // adding attachments
}
package com.banking.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Optional;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banking.Constants;
import com.banking.entity.User;
import com.banking.repository.UserRepository;
import com.banking.util.EmailTemplateUtil;

@Service
public class AdminService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private EmailTemplateUtil emailTemplateUtil;
	
	public void addOrUpdateUser(User user) {
		if(user.getId()!=null) {
			Optional<User> userOptional = userRepository.findById(user.getId());
			if(userOptional.isPresent())
			{
				User userEntity = userOptional.get();
				user.setPassword(userEntity.getPassword());
			}
		}
		else {
			emailTemplateUtil.userActivationMail(Constants.RECEIVERMAIL, user.getName());
		}
		userRepository.save(user);	
	}
	
	public Iterable<User> getAllUsers() {
		Iterable<User> iterableUsers = userRepository.findAll();
		return iterableUsers;
	}
}
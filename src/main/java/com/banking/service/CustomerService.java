package com.banking.service;

import java.text.FieldPosition;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banking.Constants;
import com.banking.entity.Account;
import com.banking.entity.Transaction;
import com.banking.entity.User;
import com.banking.model.TransactionDetailsModel;
import com.banking.repository.AccountRepository;
import com.banking.repository.TransactionRepository;
import com.banking.repository.UserRepository;
import com.banking.security.UserSession;
import com.banking.util.EmailTemplateUtil;

@Service
public class CustomerService {

	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private UserSession userSession;
	
	@Autowired
	private TransactionRepository txnRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private EmailTemplateUtil emailTemplateUtil;
	
	public void addOrUpdateAccount(Account account) {
		userSession.getCurrentUser();
		accountRepository.save(account);
	}
	
	public Iterable<Account> getAllAccounts() {
		return accountRepository.findAll();
	}
	
	public TransactionDetailsModel transfer(Transaction txn) {
		TransactionDetailsModel model = new TransactionDetailsModel();
		
		User receiverUser = userRepository.findByAccno(txn.getAccNo());
		if(receiverUser!=null) {
			User senderUser = userSession.getCurrentUser();
			if(senderUser.getBalance()!=null && senderUser.getBalance()>txn.getAmount()) {
				senderUser.setBalance(senderUser.getBalance()-txn.getAmount());
				userRepository.save(senderUser);
				receiverUser.setBalance((receiverUser.getBalance()==null?0:receiverUser.getBalance())+txn.getAmount());
				userRepository.save(receiverUser);
				txn.setAddedBy(senderUser.getId());
				txn.setReceiverUserId(receiverUser.getId());
				txn.setTimeStamp(getCurrentTimeStamp());
				model.setSuccess(true);
				txnRepository.save(txn);
				
				emailTemplateUtil.userDeActivationMail(Constants.RECEIVERMAIL, senderUser.getName(), txn.getAmount());
			}
			else {
				model.setInsufficientFunds(true);
			}
		}
		else {
			model.setAccountNotExists(true);
		}
		return model;
	}
	
	public Iterable<Transaction> getAllTransaction() {
		return txnRepository.getAllTransactionByUserId(userSession.getCurrentUser().getId());
	}
	
	private String getCurrentTimeStamp() {
		StringBuffer stringBuffer = new StringBuffer();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date now = new Date();
		simpleDateFormat.format(now, stringBuffer, new FieldPosition(0));
		return stringBuffer.toString();
	}
	
}
package com.banking.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.banking.Constants;
import com.banking.model.Mail;


@Component
public class EmailTemplateUtil {

	@Autowired
	private EmailServiceUtil emailServiceUtil;
	
	
	public void userActivationMail(String email, String name) {
		Mail mail = new Mail();
		mail.setTo(email);
		mail.setSubject("Account Activated!!!...");
		mail.setFtlFileName("accountactiation.ftl");
		
		Map<String, String> model = new HashMap<String, String>();
		model.put("name", name);
        model.put("hostnmae", Constants.HOST+"register.html");
		
        mail.setModel(model);
        emailServiceUtil.sendingMail(mail);
	}
	
	public void userDeActivationMail(String email, String name, Double amount) {
		Mail mail = new Mail();
		mail.setTo(email);
		mail.setSubject("Transaction Mail!!!...");
		mail.setFtlFileName("accountdeactiation.ftl");
		
		Map<String, String> model = new HashMap<String, String>();
		model.put("name", name);
		model.put("amount", amount+"");
        
        mail.setModel(model);
        emailServiceUtil.sendingMail(mail);
	}
}

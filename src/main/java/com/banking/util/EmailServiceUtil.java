package com.banking.util;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import com.banking.model.Mail;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@PropertySource("classpath:mail.properties")
public class EmailServiceUtil {
	@Autowired
	private JavaMailSender sender;

	@Autowired
	private Configuration freemarkerConfig;

	public void sendingMail(Mail mail) {
		MimeMessage message = sender.createMimeMessage();
		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,StandardCharsets.UTF_8.name());
			Template template = freemarkerConfig.getTemplate(mail.getFtlFileName());
			String html = FreeMarkerTemplateUtils.processTemplateIntoString(template, mail.getModel());
			helper.setTo(mail.getTo());
			//helper.setFrom("notifications@saparate.com");
			helper.setText(html, true);
			helper.setSubject(mail.getSubject());
			Map<String, String> inlineMap = mail.getInlinesMap();
			if (inlineMap != null)
				for (Map.Entry<String, String> entry : inlineMap.entrySet()) {
					helper.addInline(entry.getKey(), new ClassPathResource(entry.getValue()));
				}

			Map<String, String> attachmentsMap = mail.getAttachmentsMap();
			if (attachmentsMap != null)
				for (Map.Entry<String, String> entry : attachmentsMap.entrySet()) {
					helper.addAttachment(entry.getKey(), new ClassPathResource(entry.getValue()));
				}
			sender.send(message);
		} catch (MessagingException e) {
		} catch (IOException e) {
		} catch (TemplateException e) {
		}
	}
}

package com.banking.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Test {
	public static void main(String[] args) throws IOException, ParseException {
		String location="2, Jalan Putra Perdana 3/23, Taman Putra Perdana, 47100 Puchong, Selangor";
		 String urlString = "https://nominatim.openstreetmap.org/search?q="+location+"+&format=json&addressdetails=1";
		 final String USER_AGENT = "Mozilla/5.0";
		 URL url = new URL(urlString);
		 HttpURLConnection con = (HttpURLConnection) url.openConnection();
		 con.setRequestMethod("GET");
		 con.setRequestProperty("User-Agent", USER_AGENT);
		 BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		 String output;
		 StringBuffer response = new StringBuffer();
		 
		 while ((output = in.readLine()) != null) {
		  response.append(output);
		 }
		String s=response.toString();
		 JSONArray jSONArray=(JSONArray) new JSONParser().parse(s);
		 for (Object object : jSONArray) {
		 JSONObject jsonobj = (JSONObject) object;
		 String lat = (String) jsonobj.get("lat");
		 String lon= (String) jsonobj.get("lon");
		 System.out.println("lat::"+lat);
		 System.out.println("lon::"+lon);
		}
		 in.close();

		 //printing result from response
		 System.out.println(response.toString());
		}
}

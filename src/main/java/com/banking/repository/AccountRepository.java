package com.banking.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.banking.entity.Account;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long>{

}
package com.banking.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.banking.entity.Transaction;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Long>{
	
	@Query("SELECT t FROM Transaction t WHERE t.addedBy = :userId order by id desc")
	Iterable<Transaction> getAllTransactionByUserId(@Param("userId") Long userId);
}
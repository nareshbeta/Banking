package com.banking.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.banking.entity.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long>{
	@Query("SELECT u FROM User u WHERE u.idNumber = :clientId and u.password=:password")
	public User findByCliendIdNPassword(@Param("clientId") String clientId,@Param("password") String password);
	
	@Query("SELECT u FROM User u WHERE u.idNumber = :clientId")
	public User findByCliendId(@Param("clientId") String clientId);
	
	@Query("SELECT u FROM User u WHERE u.accNo = :accNo")
	public User findByAccno(@Param("accNo") Long accNo);
	
	@Query("SELECT u FROM User u WHERE u.accNo = :accNo and u.idNumber=:clientId")
	public User findByAccnoNClientId(@Param("accNo") Long accNo, @Param("clientId") String clientId);
}
package com.banking.security;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.banking.repository.UserRepository;


@Component
public class AdminAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider{
	
	@Autowired
	private UserRepository userRepository;

	@Override
	protected void additionalAuthenticationChecks(UserDetails arg0, UsernamePasswordAuthenticationToken arg1)
			throws AuthenticationException {
	}

	@Override
	protected UserDetails retrieveUser(String userName, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken)
			throws AuthenticationException {
		Object credentials = usernamePasswordAuthenticationToken.getCredentials();
		if(credentials != null && userName!=null) {
			String password = (String) credentials;
			if(userName.equals("admin") && password.equals("admin")) {
				boolean enabled = true, 
				accountNonExpired = true, 
				credentialsNonExpired = true, 
				accountNonLocked = true;
				List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
				authorities.add(new SimpleGrantedAuthority("admin"));
				User user = new User(userName, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities );
				return user;
			}
			else if(validateCredentials(userName, password)){
				boolean enabled = true, 
						accountNonExpired = true, 
						credentialsNonExpired = true, 
						accountNonLocked = true;
						List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
						authorities.add(new SimpleGrantedAuthority("customer"));
						User user = new User(userName, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities );
						return user;
			}
		}
		throw new UsernameNotFoundException("Unreachable code");
	}	
	
	private boolean validateCredentials(String username, String password) {
		if(username!=null && password!=null) {
			com.banking.entity.User user = userRepository.findByCliendIdNPassword(username, password);
			if(user!=null)
				return true;
		}
		return false;
	}
}
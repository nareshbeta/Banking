package com.banking.security;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler{

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		User authUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
		authorities.forEach(authority->{
			if(authority.getAuthority().equals("admin")) {
				try {
					response.sendRedirect("/banking/admin/home");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(authority.getAuthority().equals("customer")) {
				try {
					response.sendRedirect("/banking/customer/home");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
	}
}
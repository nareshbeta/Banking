package com.banking.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

@Configuration
public class AdminSecurityConfiguration extends WebSecurityConfigurerAdapter {

	private static final RequestMatcher PROTECTED_URLS_ADMIN = new OrRequestMatcher(new AntPathRequestMatcher("/admin/**"));
	private static final RequestMatcher PROTECTED_URLS_CUSTOMER = new OrRequestMatcher(new AntPathRequestMatcher("/customer/**"));

	@Autowired
	private AuthenticationFailureHandler authenticationFailureHandler;
	
	@Autowired
	private AuthenticationSuccessHandler authenticationSuccessHandler;

	private AdminAuthenticationProvider provider;

	public AdminSecurityConfiguration(final AdminAuthenticationProvider authenticationProvider) {
		super();
		this.provider = authenticationProvider;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authenticationProvider(provider).authorizeRequests()
		.requestMatchers(PROTECTED_URLS_ADMIN).hasAuthority("admin")
		.requestMatchers(PROTECTED_URLS_CUSTOMER).hasAuthority("customer")
		.and().formLogin()
				.loginPage("/login")
				.successHandler(authenticationSuccessHandler)
				//.defaultSuccessUrl("/customer/home")
				.failureHandler(authenticationFailureHandler).permitAll()
				.and().exceptionHandling().accessDeniedPage("/login?forbidden=true").and().cors()
				.and().csrf().disable().logout()
				.logoutSuccessUrl("/login?logout=true").invalidateHttpSession(true).deleteCookies("JSESSIONID")
				.permitAll();
	}
}
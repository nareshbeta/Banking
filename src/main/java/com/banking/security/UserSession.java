package com.banking.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.banking.entity.User;
import com.banking.repository.UserRepository;

@Component
public class UserSession {
	
	@Autowired
	private UserRepository userRepository;
	
	public User getCurrentUser() {
		org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User userSession = userRepository.findByCliendId(user.getUsername());
		return userSession;
	}
}

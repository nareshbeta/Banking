package com.banking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.banking.entity.User;
import com.banking.repository.UserRepository;

@RestController
public class PasswordController {
	
	@Autowired
	private UserRepository userRepo;

	@PostMapping("/checkAccnoNMyKad")
	public ResponseEntity<Boolean> checkAccountNumberNMyKad(@RequestBody User user) {
		User userCheck = userRepo.findByAccnoNClientId(user.getAccNo(), user.getIdNumber());
		if(userCheck!=null) {
			return new ResponseEntity<Boolean>(true, HttpStatus.OK);
		}
		else {
			return new ResponseEntity<Boolean>(false, HttpStatus.OK);
		}
	}
	
	@PostMapping("/updatePassword")
	public void updatePassword(@RequestBody User user) {
		User userCheck = userRepo.findByAccnoNClientId(user.getAccNo(), user.getIdNumber());
		if(userCheck!=null) {
			userCheck.setPassword(user.getPassword());
			userRepo.save(userCheck);
		}
	}
}
package com.banking.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class LoginController {
	
	@GetMapping({"/login","/"})
	public ModelAndView tilesLoginPage(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout,
			@RequestParam(value = "forbidden", required = false) String forbidden,
			@RequestParam(value = "message", required = false) String message, Model model) {

		String errorMessge = null;
		if (error != null) {
			errorMessge = message;
		}
		if (logout != null) {
			errorMessge = "You have been successfully logged out !!";
		}
		if (forbidden != null) {
			errorMessge = "Not Authorized";
		}
		model.addAttribute("errorMessge", errorMessge);
		ModelAndView view = new ModelAndView();
		view.setViewName("login");
		return view;
	}

}
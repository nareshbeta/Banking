package com.banking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.banking.entity.User;
import com.banking.service.AdminService;

@RestController
@RequestMapping("/admin")
public class AdminController {
	
	@Autowired
	private AdminService adminService;
	
	
	
	@GetMapping("/home")
	public ModelAndView getHome() {
		
		ModelAndView model = new ModelAndView();
		model.setViewName("admin");
		return model;
	}
	
	@PostMapping("/addOrUpdateUser")
	public void addOrUpdateUser(@RequestBody User user) {
		adminService.addOrUpdateUser(user);
	}
	
	@GetMapping("/allUsers")
	public ResponseEntity<Iterable<User>> getAllUsers() {
		Iterable<User> allUsers = adminService.getAllUsers();
		return new ResponseEntity<Iterable<User>>(allUsers, HttpStatus.OK);
	}
}
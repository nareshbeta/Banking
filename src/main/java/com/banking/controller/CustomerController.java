package com.banking.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.banking.entity.Account;
import com.banking.entity.Transaction;
import com.banking.entity.User;
import com.banking.model.TransactionDetailsModel;
import com.banking.security.UserSession;
import com.banking.service.AdminService;
import com.banking.service.CustomerService;

@RestController
@RequestMapping("/customer")
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private UserSession userSession;
	
	@Autowired
	private AdminService adminService;
	
	@GetMapping("/home")
	public ModelAndView getHome() {
		ModelAndView model = new ModelAndView();
		model.setViewName("customer");
		return model;
	}
	
	@PostMapping("/addOrUpdateAccount")
	public void addOrUpdateAccount(@RequestBody Account account) {
		customerService.addOrUpdateAccount(account);
	}
	
	@GetMapping("/allAccounts")
	public ResponseEntity<Iterable<Account>> getAllAccounts(){
		return new ResponseEntity<Iterable<Account>>(customerService.getAllAccounts(), HttpStatus.OK);
	}
	
	@GetMapping("/profile")
	public User getCustomerProfile() {
		return userSession.getCurrentUser();
	}
	
	@PostMapping("/updateProfile")
	public void updateCustomerProfile(@RequestBody User user) {
		adminService.addOrUpdateUser(user);
	}
	
	@PostMapping("/transfer")
	public ResponseEntity<TransactionDetailsModel> transferMoney(@RequestBody Transaction txn) {
		return new ResponseEntity<TransactionDetailsModel>(customerService.transfer(txn), HttpStatus.OK);
	}
	
	@GetMapping("/allTxns")
	public ResponseEntity<Iterable<Transaction>> getAllTransactions() {
		return new ResponseEntity<Iterable<Transaction>>(customerService.getAllTransaction(), HttpStatus.OK);
	}
}